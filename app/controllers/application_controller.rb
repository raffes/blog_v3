class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception


  def redirect_to_good_slug(object)
      redirect_to params.merge({
                    :controller => controller_name,
                    :action => params[:action],
                    :id => object.to_param,
                    :status => :moved_permanently
                  })
  end
end
