class Article < ActiveRecord::Base

	default_scope -> {order(created_at: :desc)}
	
	validates :title, presence: true,
                    length: { minimum: 3 ,maximum: 50}

    validates :text, presence: true,
    		   length: {minimum: 10 ,maximum: 250}             

    custom_slugs_with(:title)
	

end
